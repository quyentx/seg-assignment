import userlib.DomainConstraint;

public class Book extends Publication{
    /*
       TODO: Specify the attribute(s) if needed
       */

    @DomainConstraint(type = "Integer", mutable = true, optional = false, min = 1)
    private int edition;

    /*
     TODO: Specify and code constructor(s)
     */
    public Book(String title, double weight, int edition) {
        super(title, weight);
        this.edition = edition;
    }

    /*
     TODO: Specify and code getter(s), setter(s), toString, and input validation methods
     */

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public boolean repOK() {
        if (edition < 1) {
            return false;
        }else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "The book is of edition " + edition;
    }

    public static boolean isValidEdition(String input) {
        try {
            int editionValue = Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            System.out.println("Edition must be an integer, please input again!");
            return false;
        }
    }
}
