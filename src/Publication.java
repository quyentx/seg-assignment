import userlib.DomainConstraint;

public class Publication {
    /*
     TODO: Specify the attribute(s) if needed
     */
    @DomainConstraint(type = "String", mutable = true, optional = false)
    private String title;

    @DomainConstraint(type = "double", mutable = true, optional = false, min = 0.01)
    private double weight;

    /*
     TODO: Specify and code constructor(s)
     */
    public Publication(String title, double weight){
        this.title = title;
        this.weight = weight;
    }

    /*
     TODO: Specify and code getter(s), setter(s), toString, and input validation methods
     */
    public String getTitle(){
        return title;
    }

    public double getWeight(){
        return weight;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean repOK() {
        if (title == null || weight < 0.01) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "The publication is called " + title + ", which weights " + weight;
    }

    public static boolean isValidWeight(String input){
        try{
            double weightValue = Double.parseDouble(input);
            return true;
        }catch (NumberFormatException e){
            System.out.println("Weight must be a float number, please input again!");
            return false;
        }
    }

    public static boolean isValidTitle(String input){
        if(input == null){
            throw new NullPointerException();
        }else {
            return true;
        }
    }
}
