import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @overview Generates a natural sequence of non-negative numbers up to a maximum value that are divisible by 3.
 */
public class DivisibleByThreeNumbers {
    //TODO (1): declare suitable attribute(s)
    private ArrayList arraylist = new ArrayList();
    private int currentSize = arraylist.size();

    public static void main(String[] args){
        DivisibleByThreeNumbers result = new DivisibleByThreeNumbers(100);
        for(Iterator it = result.arraylist.iterator(); it.hasNext();){
            System.out.print(it.next() + " ");
        }
    }

    /**
     * @requires m >= 0
     * @effects initialise this with maximum number m
     */
    public DivisibleByThreeNumbers(int m) {
        //TODO (2): implement this method
        int i;
        for (i = 0; i < m; i++){
            if (i % 3 == 0) {
                arraylist.add(i);
            }
        }
    }

    /**
     * @effects Return a natural sequence of non-negative even numbers up to the specified maximum number that are divisible by 3.
     */
    public Iterator iterator() {
        //TODO (3):implement this method
        return new DivisibleByThreeNumberGen();
    }

    // generator class
    private class DivisibleByThreeNumberGen implements Iterator {
        //TODO (4): declare suitable attribute(s)
        private int currentIndex = 0;

        /**
         * @effects return true if exists next number that is divisible by 3
         */
        @Override
        public boolean hasNext() {
            //TODO (5):implement this method
            return currentIndex < currentSize;
        }

        /**
         *
         @effects
          *  if hasNext() = false
          *    throw NoSuchElementException
          *  else
          *    return the next number in the sequence that is divisible by 3
         */
        @Override
        public Object next() throws NoSuchElementException {
            //TODO(6): implement this method
            if(!hasNext()){
                throw new NoSuchElementException();
            }else{
                return arraylist.get(currentIndex++);
            }
        }

        @Override
        public void remove() {
            // ignore this method
        }
    }
}